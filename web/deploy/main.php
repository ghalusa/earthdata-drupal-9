<?php

/**
 * @file
 * This PHP script serves as a destination URL for Bitbucket's Webhook.
 * The script is triggered when a commit is made to a branch in the main D9 repo (e.g., master).
 * First, it pulls from the Git remote (Bitbucket).
 * Then, it runs the following commands: composer install, drush updb, drush cr, drush cim.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

// Bootstrap Drupal
$autoloader = require_once __DIR__ . '/../autoload.php';
$request = Request::createFromGlobals();
$kernel = DrupalKernel::createFromRequest($request, $autoloader, 'prod');
$kernel->boot();

// Simple test.
// $output = shell_exec('cd /var/www/html/ && /usr/bin/git status');
// var_dump($output);
// die('done!');

// Set the timezone.
date_default_timezone_set('America/New_York');

// Conditional for different paths depending on the environment, specifically the existence of the "earthdata" directory.
$base_dir = is_dir('/var/www/html/earthdata') ? '/var/www/html/earthdata' : '/var/www/html';
$change_directory = 'cd ' . $base_dir . ' && ';
// The git branch.
$git_branch = 'esds310-wireframes';
// The log label for the Watchdog logs.
$log_label = 'Earthdata Deploy Main';

// Git Pull
// Execute the command.
$git_pull_result = shell_exec($change_directory . '/usr/bin/git pull origin ' . $git_branch);

// If anything is pulled from the remote, proceed with deployment tasks.
if (!empty($git_pull_result)) {

  // Display the result and log to the database (watchdog).
  echo nl2br($git_pull_result);
  \Drupal::logger($log_label)->notice(t(nl2br($git_pull_result)));

  $commands = [
    'composer-install' => 'composer install',
    'update-database' => 'drush updb -y',
    'rebuild-caches' => 'drush cr',
    'import-configuration' => 'drush cim -y',
    'rebuild-caches' => 'drush cr',
  ];

  foreach ($commands as $key => $command) {

    // Execute the command.
    $result = shell_exec($change_directory . $command);
    // Display the result and log to the database (watchdog).
    if (!empty($result)) {
      echo nl2br($result);
      \Drupal::logger($log_label)->notice(t(nl2br($result .  "\n\n" . 'Timestamp: ' . date('Y-m-d h:i:s A'))));
    }

  }

}

if (empty($git_pull_result)) {
  // Display the message and log to the database (watchdog).
  $message = 'Deployment executed, but there was nothing to deploy.' . "\n\n" . 'Timestamp: ' . date('Y-m-d h:i:s A');
  echo nl2br($message);
  // @TODO: Re-enable logging when the script is being called by Bitbucket's Webhook (currently system cron).
  // \Drupal::logger($log_label)->error(t(nl2br($message)));
}