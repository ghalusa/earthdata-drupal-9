# Earthdata Drupal 9 Website

## Contents of this File

* Installation
* Maintainers


## Installation

### Clone the Repository

```
git clone git@bitbucket.org:ghalusa/earthdata-drupal-9.git
```

### Install Drupal, Modules, and Themes

```
composer update
```

### Place Services and Settings Files

Unpack and place settings files into htdocs/sites/default. (Feel free to ask for copies of these files.)

- services.yml
- settings.local.php

### Set up database connections in `settings.local.php`

This is just an example.

```
/**
 * Database settings
 */
$databases['default']['default'] = array (
  'database' => 'earthdata',
  'username' => 'earthdata',
  'password' => 'earthdata',
  'prefix' => '',
  'host' => 'mariadb',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
```

### Set the trusted host configuration in `settings.local.php`

```
/**
 * Trusted host configuration.
 */
$settings['trusted_host_patterns'] = [
  '^earthdata\.docker\.localhost$',
];
```

### Uncomment the settings.local.php include in settings.php

```
/**
 * Load local development override configuration, if available.
 *
 * Use settings.local.php to override variables on secondary (staging,
 * development, etc) installations of this site. Typically used to disable
 * caching, JavaScript/CSS compression, re-routing of outgoing emails, and
 * other things that should not happen on development and testing sites.
 *
 * Keep this code block at the end of this file to take full effect.
 */
#
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
```

### Check the Database

Make sure the database has no tables. If it does, then drop all tables.

### Stop/Start Docker containers

(Only if running a Dockerized environment)

### Setup the Site via Browser

- Setup the site via your browser by loading the website's home page.


### Post Installation Checks

- Check the "Status report" page (admin/reports/status) and make sure everything is in order.
- Check the "Available updates" page (admin/reports/updates) to ensure core and modlues are up-to-date, which they should be.


## Maintainers/Support

* Goran Halusa: [goran.halusa@ssaihq.com](mailto:goran.halusa@ssaihq.com)